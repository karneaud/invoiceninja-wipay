<?php
use Illuminate\Support\Facades\Schema;
use App\Models\Gateway;

class WipayPaymentLibrarySeeder extends Seeder
{
    public function run()
    {
        Eloquent::unguard();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $gateways = [
          ['name' => 'Wipay', 'provider' => 'Wipay', 'is_offsite' => true, 'sort_order' => 9, 'payment_library_id' => 1]
        ];

        foreach ($gateways as $gateway) {
            $record = Gateway::where('name', '=', $gateway['name'])->first();
            if ($record) {
                $record->fill($gateway);
                $record->save();
            } else {
                Gateway::create($gateway);
            }
        }

        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
} ?>
