<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        $configuration = new \stdClass;
        $configuration->api_key = '';
        $configuration->test_mode = true;
        $configuration->avs = true;
        $configuration->account_number = '123';
        $configuration->origin = '';
        $configuration->country_code = ['TT','JM','BB'];
        $configuration->currency = ['TTD','USD','JMD'];
        $configuration->method = ['credit_card','voucher'];
        $configuration->fee_structure = ['custom_pay','merchant_absorb','split'];

        $gateway = new \App\Models\Gateway;
        $gateway->name = 'Wipay'; 
        $gateway->key = Str::lower(Str::random(32)); 
        $gateway->provider = 'Wipay';
        $gateway->is_offsite = true;
        $gateway->fields = \json_encode($configuration);
        $gateway->visible = true;
        $gateway->site_url = "https://wipaycaribbean.com";
        $gateway->default_gateway_type_id = 1;
        $gateway->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Gateway::where('name', 'Wipay')->delete();
    }
};
