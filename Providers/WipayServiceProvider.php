<?php

namespace Modules\Wipay\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class WipayServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Wipay';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'wipay';

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
       // $this->registerConfig();
        $this->registerViews();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
        //$this->registerPaymentDriver();
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
       // $this->app->register(RouteServiceProvider::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        
        $viewPath = app_path('/../public/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');
        $this->publishes([
            $sourcePath => $viewPath
        ], ['views', $this->moduleNameLower ]);

        //config(['view.paths' => array_merge($this->getPublishableViewPaths(), [$sourcePath,$viewPath], config('view.paths'))]);\
        $this->loadViewsFrom($sourcePath, $this->moduleNameLower);
    }
    
    public function registerPaymentDriver() {
        $viewPath = resource_path('app/PaymentDrivers/' . $this->moduleName);

        $sourcePath = module_path($this->moduleName, 'PaymentDrivers');

        $this->publishes([
            $sourcePath => $viewPath
        ]);
    }
    
    

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
            $this->loadJsonTranslationsFrom($langPath);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
            $this->loadJsonTranslationsFrom(module_path($this->moduleName, 'Resources/lang'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (\Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        
        return $paths;
    }
}
