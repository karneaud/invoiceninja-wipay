<?php

/**
 * Invoice Ninja (https://invoiceninja.com).
 *
 * @link https://github.com/invoiceninja/invoiceninja source repository
 *
 * @copyright Copyright (c) 2023. Invoice Ninja LLC (https://invoiceninja.com)
 *
 * @license https://www.elastic.co/licensing/elastic-license
 */

namespace Modules\Wipay\PaymentDrivers\Wipay;

use App\Exceptions\PaymentFailed;
use App\Http\Requests\ClientPortal\Payments\PaymentResponseRequest;
use App\Jobs\Util\SystemLogger;
use App\Models\GatewayType;
use App\Models\Payment;
use App\Models\PaymentType;
use App\Models\SystemLog;
use App\PaymentDrivers\Common\MethodInterface;
use App\PaymentDrivers\WipayPaymentDriver;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Common\Exception\InvalidResponseException;

class PaymentMethod implements MethodInterface
{
    protected WipayPaymentDriver $wipay;

    public function __construct(WipayPaymentDriver $wipay)
    {
        $this->wipay = $wipay;
        $this->wipay->init();
    }

    /**
     * Show the authorization page for Wipay.
     *
     * @param array $data
     * @return \Illuminate\View\View         
     */
    public function authorizeView(array $data): View
    {
        return render('gateways.wipay.hosted.authorize', $data);
    }

    /**
     * Handle the authorization page for Wipay.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function authorizeResponse(Request $request): RedirectResponse
    {
        return redirect()->route('client.payment_methods.index');
    }

    /**
     * Payment view for the Wipay.
     *
     * @param array $data
     * @return \Illuminate\View\View         
     */
    public function paymentView(array $data): View
    {
        $response= null;
        try {
           
            $response = $this->wipay->gateway->purchase(
            [
                'transaction_id' => $data['invoices']->first()['invoice_number'],
                'return_url' => route('client.payments.response', [
                    'company_gateway_id' => $this->wipay->company_gateway->id,
                    'payment_hash' => $data['payment_hash'],
                    'payment_method_id' => GatewayType::HOSTED_PAGE,
                            ]),
                 'currency' => $data['client']->currency()->code,
                 'amount'=>$data['amount_with_fee']
            ] 
            )->send();
            
            
            if(!$response->isSuccessful()) throw new InvalidRequestException($response->getMessage(), $response->getCode()); 
            
            $data += ['url' => $response->getRedirectUrl()  ]; 
        } catch(\Throwable $e) {
             SystemLogger::dispatch(
                $e->getMessage(),
                SystemLog::CATEGORY_GATEWAY_RESPONSE,
                SystemLog::EVENT_GATEWAY_FAILURE,
                868,
                $this->wipay->client,
                $this->wipay->client->company,
            );

            throw new PaymentFailed($e->getMessage(), $e->getCode());
        }
        
        $data += ['contact'=> $data['client']->contacts()->first(), 'invoice_number' => $data['invoices']->first()['invoice_number'], 'gateway' => $this->wipay ]+$data;
        
        return view('wipay::payment', $data );
    }

    /**
     * Handle payments page for Wipay.
     *
     * @param PaymentResponseRequest $request
     * @return void
     */
    public function paymentResponse(PaymentResponseRequest $request)
    {
        $response= null;
        try {
            $response = $this->wipay->gateway->completePurchase(
                    [
                        'total' => array_sum(array_column($this->wipay->payment_hash->invoices(), 'amount')) + $this->wipay->payment_hash->fee_total,
                        'currency' => $this->wipay->client->currency()->code 
                    ] +  $request->all()
                )->send();
            if(!$response->isSuccessful()) throw new InvalidResponseException($response->getMessage(), $response->getCode()); 
            
           return  $this->processSuccessfulPayment($response->getTransactionId());
       } catch(\Exception $e) {
            $this->processUnsuccessfulPayment($e);
        }
    }

    /**
     * Handle the successful payment for Wipay.
     *
     * @param string $payment_id
     * @return RedirectResponse
     */
    public function processSuccessfulPayment(string $payment_id): RedirectResponse
    {
        $data = [
            'gateway_type_id' => GatewayType::HOSTED_PAGE,
            'amount' => array_sum(array_column($this->wipay->payment_hash->invoices(), 'amount')) + $this->wipay->payment_hash->fee_total,
            'payment_type' => PaymentType::HOSTED_PAGE,
            'transaction_reference' => $payment_id,
        ];

        $payment_record = $this->wipay->createPayment($data, Payment::STATUS_COMPLETED);

        SystemLogger::dispatch(
            ['response' => $payment_id, 'data' => $data],
            SystemLog::CATEGORY_GATEWAY_RESPONSE,
            SystemLog::EVENT_GATEWAY_SUCCESS,
            868,
            $this->wipay->client,
            $this->wipay->client->company,
        );
        return redirect()->route('client.payments.show', ['payment' => $this->wipay->encodePrimaryKey($payment_record->id)]);
    }

    /**
     * Handle unsuccessful payment for Wipay.
     *
     * @param Exception $exception
     * @throws PaymentFailed
     * @return void
     */
    public function processUnsuccessfulPayment(\Exception $exception): void
    {
        $this->wipay->sendFailureMail($exception->getMessage());

        SystemLogger::dispatch(
            $exception->getMessage(),
            SystemLog::CATEGORY_GATEWAY_RESPONSE,
            SystemLog::EVENT_GATEWAY_FAILURE,
            868,
            $this->wipay->client,
            $this->wipay->client->company,
        );

        throw new PaymentFailed($exception->getMessage(), $exception->getCode());
    }
}
