<?php
/**
 * Invoice Ninja (https://invoiceninja.com).
 *
 * @link https://github.com/invoiceninja/invoiceninja source repository
 *
 * @copyright Copyright (c) 2023. Invoice Ninja LLC (https://invoiceninja.com)
 *
 * @license https://www.elastic.co/licensing/elastic-license
 */

namespace Modules\Wipay\PaymentDrivers;

use Omnipay\Omnipay;
use Omnipay\Wipay\Gateway;
use App\PaymentDrivers\BaseDriver;
use App\Models\ClientGatewayToken;
use App\Models\GatewayType;
use App\Models\Payment;
use App\Models\PaymentHash;
use App\Models\SystemLog;
use App\PaymentDrivers\Wipay\Hosted;
use App\Utils\Traits\MakesHash;

class PaymentDriver extends BaseDriver
{
    use MakesHash;

    public $refundable = false;

    public $token_billing = false;

    public $can_authorise_credit_card = true;

    public Gateway $gateway;

    public $payment_method;

    public static $methods = [
        GatewayType::HOSTED_PAGE => Hosted::class,
    ];

    public function init(): self
    {
        
        $this->gateway = Omnipay::create(
            $this->company_gateway->gateway->provider
        );
        $this->gateway->initialize((array) $this->company_gateway->getConfig());
        return $this;
    }

    public function gatewayTypes(): array
    {
        return [
            GatewayType::HOSTED_PAGE,
        ];
    }
    
    public function setPaymentMethod($payment_method_id)
    {
        $class = self::$methods[$payment_method_id];
        $this->payment_method = new $class($this);

        return $this;
    }
    
    public function authorizeView(array $data)
    {
        return $this->payment_method->authorizeView($data);
    }

    public function authorizeResponse($request)
    {
        return $this->payment_method->authorizeResponse($request);
    }

    public function processPaymentView(array $data)
    {
        return $this->payment_method->paymentView($data);
    }

    public function processPaymentResponse($request)
    {
        return $this->payment_method->paymentResponse($request);
    }
}
