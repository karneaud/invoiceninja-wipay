@extends('portal.ninja2020.layout.payments', ['gateway_title' => 'Wipay Payments', 'card_title' => ctrans('texts.credit_card')])

@section('gateway_content')
    <div class="col-md-12">
      <div class="px-4 py-5 border-b border-gray-200 sm:px-6 container">
        <h2 class="my-4">Payment Processing Information</h2>
        <blockquote class="my-4 text-left">
          <p>Use Wipay to transfer funds from either your Wipay account or with a credit card. On clicking the "PAY NOW" button you will be taken to the Wipay Payment Gateway to fill out and complete the payment transaction and then be returned to this website.</p>
        </blockquote>
        <div class="row">
        <p>&nbsp;</p>
        </div>
        @component('portal.ninja2020.components.general.card-element', ['title' => ctrans('texts.payment_type')])
            Credit Card via Wipay
        @endcomponent

        @include('portal.ninja2020.gateways.includes.payment_details')

        @include('portal.ninja2020.gateways.includes.pay_now',['form'=> 'wipay-redirect'])
@endsection
@section('gateway_footer')
  <script>

      document.getElementById('pay-now').addEventListener('click', function() {
        window.location.href = "{!! htmlspecialchars_decode($url) !!}";
      });

      
  </script>
@endsection
