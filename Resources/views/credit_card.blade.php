@extends('payments.payment_method')

@section('payment_details')

    {!! Former::open($url) !!}
    <div class="col-md-12">
      <div class="container-fluid text-center">
        <h3>Payment Processing Information</h3>
      <blockquote class="text-left">
        <p>Use Wipay to transfer funds from either your Wipay account or with a credit card. On clicking the "PAY NOW" button you will be taken to the Wipay Payment Gateway to fill out and complete the payment transaction and then be returned to this website.</p>
      </blockquote>
      <div class="row">
        <div class="col-md-6">
          <div class="well well-sm">
            <address class="text-center">
              <strong>{{$contact->first_name}} {{$contact->last_name}}</strong><br/>
               <abbr title="Email">E:</abbr> {{$contact->email}}<br/>
                <abbr title="Phone">P:</abbr> {{$contact->phone}}</br>
            </address>
        </div>
      </div>
        <div class="col-md-6">
          <div class="well well-sm">
            <address class="text-center">
                <strong>Invoice #:</strong> {{$order_id}}<br/>
                <strong>Transaction #:</strong> {{$transactionId}}<br/>
            </address>
        </div>
      </div>
    </div>
      <input type='hidden' name='total' value="{{$total}}" />
      <input type='hidden' name='phone' value="{{$contact->phone}}" />
      <input type='hidden' name='order_id' value="{{$order_id}}" />
      <input type='hidden' name='developer_id' value="{{$developer_id}}" />
      <input type="hidden" name="name" value="{{$contact->first_name}} {{$contact->last_name}}"/>
      <input type="hidden" name="email" value="{{$contact->email}}"/>
      <input type="hidden" name="return_url" value="{{$return_url}}"/>
      <p>&nbsp;</p>

      @if (isset($total) && $client && $account->showTokenCheckbox())
          <input id="token_billing" type="checkbox" name="token_billing" {{ $account->selectTokenCheckbox() ? 'CHECKED' : '' }} value="1" style="margin-left:0px; vertical-align:top">
          <label for="token_billing" class="checkbox" style="display: inline;">{{ trans('texts.token_billing_braintree_paypal') }}</label>
          <span class="help-block" style="font-size:15px">
              {!! trans('texts.token_billing_secure', ['link' => link_to('https://www.firstatlanticcommerce.com/', 'Wipay', ['target' => '_blank'])]) !!}
          </span>
      @endif

      <p>&nbsp;</p>

      <div class="text-center">
          {!! Button::success(strtoupper(trans('texts.pay_now') . ' - ' . $account->formatMoney($total, $client, CURRENCY_DECORATOR_CODE)  ))
                              ->submit()->large() !!}
      </center>
      </div>

  </div>
    {!! Former::close() !!}

@stop
